FROM ubuntu:20.04

RUN apt-get update && \
    apt-get install -y vim nginx telnet sudo curl supervisor wget tar

COPY /config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Install NodeJS & NPM
RUN curl -fsSL https://deb.nodesource.com/setup_14.x | sudo -E bash - && \
    sudo apt-get install -y nodejs && \
    sudo npm install --global yarn

RUN wget https://golang.org/dl/go1.17.2.linux-amd64.tar.gz && \
    tar -C /usr/local -xzf go1.17.2.linux-amd64.tar.gz && \
    rm -rf go1.17.2.linux-amd64.tar.gz && \
    echo "export PATH=$PATH:/usr/local/go/bin" > .bashrc  

SHELL [ "/bin/bash", "-c"]

RUN source /.bashrc

RUN mkdir -p /var/www/html/frontend

COPY /frontend /var/www/html/frontend

WORKDIR /var/www/html/frontend

RUN yarn install

EXPOSE 8080

CMD /usr/bin/supervisord -c /etc/supervisor/conf.d/supervisord.conf