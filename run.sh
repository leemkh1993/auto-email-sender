#!/bin/bash

set -e 

if [[ $1 == "" ]] | [[ $1 != 'build' && $1 != 'run' && $1 != 'rm' && $1 != 'rmi' && $1 != 'ssh' ]];
then
    echo "Usage: $0 [build,run,rm,rmi,ssh]"
    exit 1
fi

Image="auto-email-sender"

if [[ $1 == 'build' ]];
then
    docker build -f auto-email-sender.dockerfile -t $Image .
    exit 0
fi

if [[ $1 == 'run' ]];
then
    docker run -ditp 8080:8080 $Image
    exit 0
fi

if [[ $1 == 'rm' ]];
then
    ID=$(docker ps --filter "ancestor=$Image" --format {{.ID}})
    docker rm -f $ID
    exit 0
fi

if [[ $1 == 'rmi' ]];
then
    docker rmi -f $Image
    exit 0
fi

if [[ $1 == 'ssh' ]];
then
    ID=$(docker ps --filter "ancestor=$Image" --format {{.ID}})
    docker exec -it -u root $ID bash
    exit 0
fi