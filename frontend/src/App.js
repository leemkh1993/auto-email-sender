import logo from './logo.svg';
import './App.css';
import { Editor } from '@tinymce/tinymce-react';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <div>
          <Editor />
        </div>
      </header>
    </div>
  );
}

export default App;
