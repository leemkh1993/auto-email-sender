package main

import (
	"fmt"
	"log"
	"net/smtp"
)

type Email struct {
	sender   string
	receiver string
	body     string
}

func main() {
	fmt.Printf("Auto sender")

	// Connect to SMTP server
	smtpConnect, err := smtp.Dial("smtp.gmail.com:25")
	if err != nil {
		log.Fatal(err)
	}

	// Set sender's email, Mail function return error
	if err := smtpConnect.Mail("eelp1234a@gmail.com"); err != nil {
		log.Fatal(err)
	}

	// Set sender's email, Mail function return error
	if err := smtpConnect.Rcpt("marco.lee@hotmail.com"); err != nil {
		log.Fatal(err)
	}

	// Send the email body
	eBody, err := smtpConnect.Data()

	if err != nil {
		log.Fatal(err)
	}

	_, err = fmt.Fprintf(eBody, "Test email")

	if err != nil {
		log.Fatal(err)
	}

	if err := eBody.Close(); err != nil {
		log.Fatal(err)
	}

	// Close connection
	if err := smtpConnect.Close(); err != nil {
		log.Fatal(err)
	}
}
