package helper

import (
	"log"

	excelizeV2 "github.com/360EntSecGroup-Skylar/excelize/v2"
)

func readExcel(path string) {
	// Open the excel file
	excel, err := excelizeV2.OpenFile(path)

	if err != nil {
		log.Fatal(err)
		return
	}

	// Get the rows
	rows, err := excel.GetRows("Sheet1")

	if err != nil {
		log.Fatal(err)
		return
	}

	return rows
}
